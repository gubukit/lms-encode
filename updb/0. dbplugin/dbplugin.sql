/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : dbplugin

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 09/07/2020 13:46:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pmt_jurusan
-- ----------------------------
DROP TABLE IF EXISTS `pmt_jurusan`;
CREATE TABLE `pmt_jurusan`  (
  `id` int(11) NOT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jml_siswa` int(11) NULL DEFAULT NULL,
  `jml_kelas` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pmt_prodi
-- ----------------------------
DROP TABLE IF EXISTS `pmt_prodi`;
CREATE TABLE `pmt_prodi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jurusan` int(11) NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pmt_siswa
-- ----------------------------
DROP TABLE IF EXISTS `pmt_siswa`;
CREATE TABLE `pmt_siswa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_prodi` int(11) NULL DEFAULT NULL,
  `bin_1` double(11, 2) NULL DEFAULT NULL,
  `big_1` double(11, 2) NULL DEFAULT NULL,
  `mat_1` double(11, 2) NULL DEFAULT NULL,
  `ipa_1` double(11, 2) NULL DEFAULT NULL,
  `ips_1` double(11, 2) NULL DEFAULT NULL,
  `bin_2` double(11, 2) NULL DEFAULT NULL,
  `big_2` double(11, 2) NULL DEFAULT NULL,
  `mat_2` double(11, 2) NULL DEFAULT NULL,
  `ipa_2` double(11, 2) NULL DEFAULT NULL,
  `ips_2` double(11, 2) NULL DEFAULT NULL,
  `bin_3` double(11, 2) NULL DEFAULT NULL,
  `big_3` double(11, 2) NULL DEFAULT NULL,
  `mat_3` double(11, 2) NULL DEFAULT NULL,
  `ipa_3` double(11, 2) NULL DEFAULT NULL,
  `ips_3` double(11, 2) NULL DEFAULT NULL,
  `bin_4` double(11, 2) NULL DEFAULT NULL,
  `big_4` double(11, 2) NULL DEFAULT NULL,
  `mat_4` double(11, 2) NULL DEFAULT NULL,
  `ipa_4` double(11, 2) NULL DEFAULT NULL,
  `ips_4` double(11, 2) NULL DEFAULT NULL,
  `bin_5` double(11, 2) NULL DEFAULT NULL,
  `big_5` double(11, 2) NULL DEFAULT NULL,
  `mat_5` double(11, 2) NULL DEFAULT NULL,
  `ipa_5` double(11, 2) NULL DEFAULT NULL,
  `ips_5` double(11, 2) NULL DEFAULT NULL,
  `bin_total` double(11, 2) NULL DEFAULT NULL,
  `big_total` double(11, 2) NULL DEFAULT NULL,
  `mat_total` double(11, 0) NULL DEFAULT NULL,
  `ipa_total` double(11, 0) NULL DEFAULT NULL,
  `ips_total` double(11, 0) NULL DEFAULT NULL,
  `total` double(11, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
