/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : dbpusat

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 28/07/2020 14:08:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for x_absen_guru
-- ----------------------------
DROP TABLE IF EXISTS `x_absen_guru`;
CREATE TABLE `x_absen_guru`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XGuru` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('H','S','I','T','A') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_absen_siswa
-- ----------------------------
DROP TABLE IF EXISTS `x_absen_siswa`;
CREATE TABLE `x_absen_siswa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XGuru` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('H','S','I','T','A') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_bukukerja
-- ----------------------------
DROP TABLE IF EXISTS `x_bukukerja`;
CREATE TABLE `x_bukukerja`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XGuru` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_jenis` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_bukukerja_jenis
-- ----------------------------
DROP TABLE IF EXISTS `x_bukukerja_jenis`;
CREATE TABLE `x_bukukerja_jenis`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of x_bukukerja_jenis
-- ----------------------------
INSERT INTO `x_bukukerja_jenis` VALUES (1, '1. Analisis KI KD');
INSERT INTO `x_bukukerja_jenis` VALUES (2, '2. Pemetaan KD');
INSERT INTO `x_bukukerja_jenis` VALUES (3, '3. Silabus');
INSERT INTO `x_bukukerja_jenis` VALUES (4, '4. Rencana pekan efektif');
INSERT INTO `x_bukukerja_jenis` VALUES (5, '5. Program tahunan');
INSERT INTO `x_bukukerja_jenis` VALUES (6, '6. Program semester');
INSERT INTO `x_bukukerja_jenis` VALUES (7, '7. Rencana Pelaksanaan Pembelajaran');
INSERT INTO `x_bukukerja_jenis` VALUES (8, '8. Kriteria ketentuan minimal');

-- ----------------------------
-- Table structure for x_jurnal
-- ----------------------------
DROP TABLE IF EXISTS `x_jurnal`;
CREATE TABLE `x_jurnal`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XGuru` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_rencana` int(11) NULL DEFAULT NULL,
  `tgl` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_jurnal_rencana
-- ----------------------------
DROP TABLE IF EXISTS `x_jurnal_rencana`;
CREATE TABLE `x_jurnal_rencana`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XGuru` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL COMMENT '1-30',
  `rencana` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO versi (versi) VALUES ("2.7");

ALTER TABLE `dbpusat`.`wp_siswa` 
ADD COLUMN `last_online` datetime(0) NULL AFTER `file`;