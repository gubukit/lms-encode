ALTER TABLE `dbpusat`.`wp_siswa` 
ADD COLUMN `file` varchar(100) NULL AFTER `XInfoNIK`;

DROP TABLE IF EXISTS `x_modul`;
CREATE TABLE `x_modul`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XLevel` varchar(25) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJudul` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XGuru` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `XTglBuat` date NOT NULL,
  `tingkat` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `npsn` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `izin` int(2) NOT NULL DEFAULT 0,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `statusupload` int(1) NOT NULL,
  `thnajar` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `semester` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `XKompetensi` enum('1','2','3','4','5','6','7','8','9','10') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `is_ukbm` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `Urut`(`Urut`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for x_modul_log
-- ----------------------------
DROP TABLE IF EXISTS `x_modul_log`;
CREATE TABLE `x_modul_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_modul` int(11) NULL DEFAULT NULL,
  `xnomerujian` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

ALTER TABLE `dbpusat`.`x_modul` 
ADD COLUMN `XIsi` text NULL AFTER `XJudul`; 

ALTER TABLE `dbpusat`.`x_kompetensi` 
MODIFY COLUMN `k1` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `max_kompetensi`,
MODIFY COLUMN `k2` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k1`,
MODIFY COLUMN `k3` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k2`,
MODIFY COLUMN `k4` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k3`,
MODIFY COLUMN `k5` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k4`,
MODIFY COLUMN `k6` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k5`,
MODIFY COLUMN `k7` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k6`,
MODIFY COLUMN `k8` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k7`,
MODIFY COLUMN `k9` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k8`,
MODIFY COLUMN `k10` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `k9`;

INSERT INTO versi (versi) VALUES ("2.2");
