INSERT INTO versi (versi) VALUES ("2.6");

/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : dbpusat

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 25/07/2020 09:29:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for x_jawaban_ktr
-- ----------------------------
DROP TABLE IF EXISTS `x_jawaban_ktr`;
CREATE TABLE `x_jawaban_ktr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `IDPaketsoal` int(11) NULL DEFAULT NULL,
  `IDSoal` int(11) NULL DEFAULT NULL,
  `type` enum('file','text') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jawaban` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nilai` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_kompetensi_ktr
-- ----------------------------
DROP TABLE IF EXISTS `x_kompetensi_ktr`;
CREATE TABLE `x_kompetensi_ktr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_xkompetensi` int(11) NULL DEFAULT NULL,
  `k1` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k2` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k3` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k4` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k5` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k6` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k7` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k8` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k9` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k10` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_paketsoal_ktr
-- ----------------------------
DROP TABLE IF EXISTS `x_paketsoal_ktr`;
CREATE TABLE `x_paketsoal_ktr`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XLevel` varchar(25) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJumSoal` int(11) NULL DEFAULT NULL,
  `XGuru` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `XTglBuat` date NOT NULL,
  `tingkat` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `npsn` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `izin` int(2) NOT NULL DEFAULT 0,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `statusupload` int(1) NOT NULL,
  `thnajar` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `semester` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `XKompetensi` enum('1','2','3','4','5','6','7','8','9','10') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `is_ukbm` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `Urut`(`Urut`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for x_soal_ktr
-- ----------------------------
DROP TABLE IF EXISTS `x_soal_ktr`;
CREATE TABLE `x_soal_ktr`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `IDPaketSoal` int(11) NOT NULL,
  `XJenisSoal` int(11) NOT NULL DEFAULT 1,
  `XNomerSoal` int(50) NOT NULL,
  `XRagu` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTanya` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XAudioTanya` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XVideoTanya` mediumtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `npsn` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `XKodeMapel`(`IDPaketSoal`) USING BTREE,
  INDEX `XNomerSoal`(`XNomerSoal`) USING BTREE,
  FULLTEXT INDEX `XTanya`(`XTanya`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for x_ujian_ktr
-- ----------------------------
DROP TABLE IF EXISTS `x_ujian_ktr`;
CREATE TABLE `x_ujian_ktr`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `IDPaketsoal` int(11) NOT NULL,
  `XTglUjian` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `XNilai` varchar(11) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `XNomerUjian`(`XNIK`) USING BTREE,
  INDEX `XKodeSoal`(`IDPaketsoal`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

insert into x_kompetensi_ktr (id_xkompetensi) select id from x_kompetensi