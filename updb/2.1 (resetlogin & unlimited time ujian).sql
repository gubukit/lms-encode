ALTER TABLE `dbpusat`.`wp_pengaturan` 
ADD COLUMN `XResetLogin` tinyint(4) NULL DEFAULT 1 AFTER `XPINOut`;

ALTER TABLE `dbpusat`.`wp_ujian` 
ADD COLUMN `XUnlimitedTime` tinyint(2) NULL DEFAULT 0 AFTER `semester`;

INSERT INTO versi (versi) VALUES ("2.1");

ALTER TABLE `dbpusat`.`wp_paketsoal` 
ADD COLUMN `is_ukbm` tinyint(4) NULL DEFAULT 0 AFTER `XKompetensi`;