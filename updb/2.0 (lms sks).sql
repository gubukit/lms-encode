ALTER TABLE `tbl_pengumuman` ADD COLUMN `tgl` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) AFTER `npsn`;

ALTER TABLE `wp_level` ADD COLUMN `Sem1` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `XSN`;

ALTER TABLE `wp_level` ADD COLUMN `Sem2` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `Sem1`;

ALTER TABLE `wp_level` ADD COLUMN `Urutan` int(11) NULL DEFAULT NULL AFTER `Sem2`;

ALTER TABLE `wp_mapel` ADD COLUMN `XKelompok` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT '1' AFTER `XNamaMapel`;

ALTER TABLE `wp_nilai` ADD COLUMN `XNilaiTambah` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `XTotalNilai`;

ALTER TABLE `wp_nilai` ADD COLUMN `XNilaiFinal` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `XNilaiTambah`;

ALTER TABLE `wp_paketsoal` ADD COLUMN `XPrimary` tinyint(2) NULL DEFAULT 1 AFTER `hideanalisa`;

ALTER TABLE `wp_paketsoal` ADD COLUMN `id_xkompetensi` int(11) NULL DEFAULT NULL AFTER `XPrimary`;

ALTER TABLE `wp_pengaturan` ADD COLUMN `XSafeMode` tinyint(4) NULL DEFAULT 0 AFTER `XTipeUKBM`;

ALTER TABLE `wp_pengaturan` ADD COLUMN `XPIN` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `XSafeMode`;

ALTER TABLE `wp_pengaturan` ADD COLUMN `XSafeModeOut` tinyint(4) NULL DEFAULT NULL AFTER `XPIN`;

ALTER TABLE `wp_pengaturan` ADD COLUMN `XPINOut` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `XSafeModeOut`;

CREATE TABLE `x_kompetensi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XSN` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTingkat` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSemester` tinyint(2) NULL DEFAULT NULL,
  `XKKM` float NOT NULL,
  `max_kompetensi` int(11) NOT NULL,
  `k1` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k2` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k3` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k4` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k5` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k6` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k7` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k8` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k9` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k10` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

CREATE TABLE `x_nilai`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `XKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `XSemester` tinyint(2) NULL DEFAULT NULL,
  `Tipe` enum('PENGETAHUAN','KETERAMPILAN','SIKAP','SPIRITUAL') CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k1` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k2` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k3` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k4` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k5` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k6` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k7` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k8` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k9` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `k10` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

CREATE TABLE `x_nilai_sikap`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NilaiAwal` int(11) NULL DEFAULT NULL,
  `NilaiAkhir` int(11) NULL DEFAULT NULL,
  `Huruf` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Huruf2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

CREATE TABLE `x_siswa_kompetensi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XSemester` tinyint(2) NULL DEFAULT NULL,
  `XKompetensi` enum('1','2','3','4','5','6','7','8','9','10') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `request` tinyint(2) NULL DEFAULT 0,
  `date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

CREATE TABLE `x_siswa_kompetensi_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XSemester` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKompetensi` enum('1','2','3','4','5','6','7','8','9','10') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

CREATE TABLE `x_siswa_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XLevel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `XSemester` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

ALTER TABLE `wp_paketsoal` 
ADD COLUMN `XKompetensi` enum('1','2','3','4','5','6','7','8','9','10') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1' AFTER `id_xkompetensi`;

DROP TABLE IF EXISTS `x_kkm`;
CREATE TABLE `x_kkm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `XKodeMapel` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XTingkat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKKM` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for x_nilai_sikap
-- ----------------------------
DROP TABLE IF EXISTS `x_nilai_sikap`;
CREATE TABLE `x_nilai_sikap`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NilaiAwal` int(11) NULL DEFAULT NULL,
  `NilaiAkhir` int(11) NULL DEFAULT NULL,
  `Huruf` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Huruf2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of x_nilai_sikap
-- ----------------------------
INSERT INTO `x_nilai_sikap` VALUES (1, 0, 74, 'D', NULL);
INSERT INTO `x_nilai_sikap` VALUES (2, 75, 82, 'C', NULL);
INSERT INTO `x_nilai_sikap` VALUES (3, 83, 90, 'B', NULL);
INSERT INTO `x_nilai_sikap` VALUES (4, 91, 100, 'A', NULL);

INSERT INTO versi (versi) VALUES ("2.0");

ALTER TABLE `wp_paketsoal` 
DROP COLUMN `id_xkompetensi`;

ALTER TABLE `x_kkm` 
ADD COLUMN `XBeban` int(11) NULL AFTER `XKKM`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wp_level
-- ----------------------------
DROP TABLE IF EXISTS `wp_level`;
CREATE TABLE `wp_level`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XLevel` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Xstatus` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSN` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Sem1` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `Sem2` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `Urutan` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Urut`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_level
-- ----------------------------
INSERT INTO `wp_level` VALUES (2, 'XI', '1', 'SMU', '3', '4', 2);
INSERT INTO `wp_level` VALUES (3, 'XII', '1', 'SMU', '5', '6', 3);
INSERT INTO `wp_level` VALUES (4, 'XII', '1', 'SMK', '5', '6', 3);
INSERT INTO `wp_level` VALUES (5, 'XI', '1', 'SMK', '3', '4', 2);
INSERT INTO `wp_level` VALUES (6, 'X', '1', 'SMK', '1', '2', 1);
INSERT INTO `wp_level` VALUES (7, 'XII', '1', 'MA', '5', '6', 3);
INSERT INTO `wp_level` VALUES (8, 'XI', '1', 'MA', '3', '4', 2);
INSERT INTO `wp_level` VALUES (13, 'X', '1', 'MA', '1', '2', 1);
INSERT INTO `wp_level` VALUES (14, 'IX', '1', 'SMP', '5', '6', 3);
INSERT INTO `wp_level` VALUES (15, 'VIII', '1', 'SMP', '3', '4', 2);
INSERT INTO `wp_level` VALUES (16, 'VII', '1', 'SMP', '1', '2', 1);
INSERT INTO `wp_level` VALUES (17, 'IX', '1', 'MTS', '5', '6', 3);
INSERT INTO `wp_level` VALUES (18, 'VIII', '1', 'MTS', '3', '4', 2);
INSERT INTO `wp_level` VALUES (19, 'VII', '1', 'MTS', '1', '2', 1);
INSERT INTO `wp_level` VALUES (20, 'VI', '1', 'MI', NULL, NULL, NULL);
INSERT INTO `wp_level` VALUES (21, 'V', '1', 'MI', NULL, NULL, NULL);
INSERT INTO `wp_level` VALUES (22, 'IV', '1', 'MI', NULL, NULL, NULL);
INSERT INTO `wp_level` VALUES (23, 'X', '1', 'SMU', '1', '2', 1);

SET FOREIGN_KEY_CHECKS = 1;