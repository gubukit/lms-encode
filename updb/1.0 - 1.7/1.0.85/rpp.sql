/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : dbpusat

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 28/07/2019 13:26:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rpp
-- ----------------------------
DROP TABLE IF EXISTS `rpp`;
CREATE TABLE `rpp`  (
  `nomorx` int(11) NOT NULL AUTO_INCREMENT,
  `kd` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `judul` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `editor` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `user` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `semester` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `thnajar` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `materiguru` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `materisiswa` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tingkat` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nomorx`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rpp_master
-- ----------------------------
DROP TABLE IF EXISTS `rpp_master`;
CREATE TABLE `rpp_master`  (
  `nomorx` int(11) NOT NULL AUTO_INCREMENT,
  `kd` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `judul` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `editor` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `user` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `semester` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `thnajar` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `materiguru` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `materisiswa` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tingkat` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nomorx`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
