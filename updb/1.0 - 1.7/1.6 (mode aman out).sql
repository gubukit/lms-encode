ALTER TABLE `wp_pengaturan` 
ADD COLUMN `XSafeModeOut` tinyint(4) NULL AFTER `XPIN`,
ADD COLUMN `XPINOut` varchar(50) NULL AFTER `XSafeModeOut`;

ALTER TABLE `tbl_pengumuman` 
ADD COLUMN `tgl` datetime(0) NULL DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP(0) AFTER `npsn`;

INSERT INTO versi (versi) VALUES ("1.6");