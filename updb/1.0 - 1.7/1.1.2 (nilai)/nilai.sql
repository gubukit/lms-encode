/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : dbpusat

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-09-06 14:18:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nilai
-- ----------------------------
DROP TABLE IF EXISTS `nilai`;
CREATE TABLE `nilai` (
  `id`  int(11) NOT NULL AUTO_INCREMENT ,
  `nik_siswa` varchar(50) DEFAULT NULL,
  `mapel` varchar(50) DEFAULT NULL,
  `kelas` varchar(30) DEFAULT NULL,
  `guru`  varchar(50) DEFAULT NULL,
  `id_kd` varchar(11) DEFAULT NULL,
  `3.1` int(11) DEFAULT NULL,
  `3.1_extra` int(11) DEFAULT NULL,
  `3.1_final` int(11) DEFAULT NULL,
  `catatan` text,
  `4.1` int(11) DEFAULT NULL,
  `1.1` int(11) DEFAULT NULL,
  `2.1` int(11) DEFAULT NULL,
  `status` enum('Y','N') DEFAULT NULL COMMENT 'y= sesuai kkm, n = tidk sesuai kkm',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
