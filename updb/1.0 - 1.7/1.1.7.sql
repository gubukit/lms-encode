ALTER TABLE `dbpusat`.`wp_paketsoal` 
MODIFY COLUMN `main` int(11) NOT NULL DEFAULT 1 AFTER `statusupload`;

ALTER TABLE `dbpusat`.`wp_jawaban` ADD INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_jawaban` ADD INDEX `XTokenUjian`(`XTokenUjian`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_jawaban` ADD INDEX `XUserJawab`(`XUserJawab`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_nilai` ADD INDEX `XNomerUjian`(`XNomerUjian`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_nilai` ADD INDEX `XTokenUjian`(`XTokenUjian`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_nilai` ADD INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_nilai` ADD INDEX `XKodeKelas`(`XKodeKelas`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_soal` ADD INDEX `XKodeMapel`(`XKodeMapel`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_soal` ADD INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_soal` ADD INDEX `XKodeKelas`(`XKodeKelas`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_soal` ADD INDEX `XNomerSoal`(`XNomerSoal`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_soal` ADD INDEX `XKunciJawaban`(`XKunciJawaban`) USING BTREE;


ALTER TABLE `dbpusat`.`wp_soal` ADD INDEX `bobot`(`bobot`) USING BTREE;

ALTER TABLE `dbpusat`.`wp_soal` ADD FULLTEXT INDEX `XTanya`(`XTanya`);

ALTER TABLE `dbpusat`.`wp_mapel` 
ADD COLUMN `icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `npsn`;

-- ----------------------------
-- Table structure for wp_modul
-- ----------------------------
DROP TABLE IF EXISTS `wp_modul`;
CREATE TABLE `wp_modul`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XLevel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTingkat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XKodeJurusan` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTipe` enum('PAKET','PELAJARAN') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Cover` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `File` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `XTglBuat` date NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;
