/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : dbpusat

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 10/07/2019 13:44:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kd_jawaban
-- ----------------------------
DROP TABLE IF EXISTS `kd_jawaban`;
CREATE TABLE `kd_jawaban`  (
  `Urutan` int(11) NOT NULL AUTO_INCREMENT,
  `Urut` int(11) NOT NULL,
  `XNomerSoal` int(11) NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL,
  `XTokenUjian` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XA` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XB` int(1) NOT NULL,
  `XC` int(1) NOT NULL,
  `XD` int(1) NOT NULL,
  `XE` int(1) NOT NULL,
  `XJawaban` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTemp` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawabanEsai` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJawab` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilaiJawab` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilaiEsai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XRagu` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XMulai` float NOT NULL,
  `XPutar` int(11) NOT NULL,
  `XMulaiV` float NOT NULL,
  `XPutarV` int(11) NOT NULL,
  `XTglJawab` date NOT NULL,
  `XJamJawab` time(0) NOT NULL,
  `XKunciJawaban` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XUserJawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Campur` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSemester` int(1) NOT NULL,
  `XUserPeriksa` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTglPeriksa` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJamPeriksa` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bobot` varchar(6) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urutan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 83954 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kd_nilai
-- ----------------------------
DROP TABLE IF EXISTS `kd_nilai`;
CREATE TABLE `kd_nilai`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTgl` date NOT NULL,
  `XJumSoal` int(11) NOT NULL,
  `XBenar` int(11) NOT NULL,
  `XSalah` int(11) NOT NULL,
  `XNilai` varchar(11) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XPersenPil` float NOT NULL,
  `XPersenEsai` float NOT NULL,
  `XEsai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTotalNilai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XPIL` int(5) NOT NULL,
  `totsatu` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totdua` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tottiga` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totempat` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totlima` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totenam` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tottujuh` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totdela` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totsembi` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totsepul` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totsatux` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totduax` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tottigax` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totempatx` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totlimax` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totenamx` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tottujuhx` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totdelax` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totsembix` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totsepulx` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kd_paketsoal
-- ----------------------------
DROP TABLE IF EXISTS `kd_paketsoal`;
CREATE TABLE `kd_paketsoal`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XNoKompetensi` int(11) NULL DEFAULT NULL,
  `XPilGanda` int(11) NOT NULL,
  `XEsai` int(11) NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJumPilihan` int(11) NOT NULL DEFAULT 5,
  `XJumSoal` int(11) NOT NULL,
  `XSemua` enum('Y','T') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'T',
  `XStatusSoal` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `XTglBuat` date NOT NULL,
  `npsn` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `izin` int(2) NOT NULL DEFAULT 0,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `statusupload` int(1) NOT NULL,
  `XLamaUjian` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `Urut`(`Urut`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kd_soal
-- ----------------------------
DROP TABLE IF EXISTS `kd_soal`;
CREATE TABLE `kd_soal`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL DEFAULT 1,
  `XLevel` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNomerSoal` int(50) NOT NULL,
  `XRagu` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XTanya` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XAudioTanya` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XVideoTanya` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab1` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab2` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab3` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab4` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XJawab5` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XKunciJawaban` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XKategori` int(11) NOT NULL DEFAULT 1,
  `XAcakSoal` enum('A','T') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  `XAcakOpsi` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `XStatus` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahas` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `npsn` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for kompetensi
-- ----------------------------
DROP TABLE IF EXISTS `kompetensi`;
CREATE TABLE `kompetensi`  (
  `nomor` int(11) NOT NULL AUTO_INCREMENT,
  `mapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kelas` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `jenis` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kode` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kompetensi` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ringkas` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `guru` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `semester` int(3) NOT NULL,
  `thnajar` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `status` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'N',
  PRIMARY KEY (`nomor`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_pengaturan
-- ----------------------------
ALTER TABLE `wp_pengaturan` 
ADD COLUMN `XTipeUKBM` enum('PAKET','SKS') CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `panitia`;

SET FOREIGN_KEY_CHECKS = 1;
