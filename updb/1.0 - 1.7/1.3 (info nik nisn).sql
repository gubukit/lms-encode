ALTER TABLE `wp_siswa` 
ADD COLUMN `XInfoNISN` varchar(50) NULL AFTER `bantuan`,
ADD COLUMN `XInfoNIK` varchar(50) NULL AFTER `XInfoNISN`;

-- ----------------------------
-- Table structure for wp_ujian_kelas
-- ----------------------------
DROP TABLE IF EXISTS `wp_ujian_kelas`;
CREATE TABLE `wp_ujian_kelas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ujian` int(11) NULL DEFAULT NULL,
  `XKodeKelas` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO versi (versi) VALUES ("1.3");
