ALTER TABLE `dbpusat`.`wp_jawaban` 
ADD COLUMN `XNilaiEsaiFix` tinyint(2) NULL DEFAULT 0 AFTER `idserver`;

-- ----------------------------
-- Table structure for versi
-- ----------------------------
DROP TABLE IF EXISTS `versi`;
CREATE TABLE `versi`  (
  `versi` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;


INSERT INTO versi (versi) VALUES ("1.1.4");