ALTER TABLE `dbpusat`.`kompetensi` 
ADD COLUMN `kodepaket` varchar(10) NULL AFTER `kode`;

ALTER TABLE `dbpusat`.`wp_admin` 
DROP COLUMN `thnajar`,
ADD COLUMN `thnajar` varchar(20) NOT NULL DEFAULT '' AFTER `lokasi_kode`;

ALTER TABLE `dbpusat`.`wp_paketsoal` 
ADD COLUMN `thnajar` varchar(20) NOT NULL DEFAULT '' AFTER `main`,
ADD COLUMN `semester` varchar(20) NOT NULL DEFAULT '' AFTER `thnajar`;

ALTER TABLE `dbpusat`.`wp_ujian` 
ADD COLUMN `thnajar` varchar(20) NOT NULL DEFAULT '' AFTER `kodebersama`,
ADD COLUMN `semester` varchar(20) NOT NULL DEFAULT '' AFTER `thnajar`;

ALTER TABLE `dbpusat`.`tbl_jadwal_pelajaran` 
ADD COLUMN `thnajar` varchar(20) NOT NULL DEFAULT '' AFTER `kkm`,
ADD COLUMN `semester` varchar(20) NOT NULL DEFAULT '' AFTER `thnajar`;