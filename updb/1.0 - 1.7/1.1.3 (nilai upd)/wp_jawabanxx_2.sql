/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.2.36
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : 192.168.2.36:3306
 Source Schema         : dbpusat

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 18/09/2019 13:16:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wp_jawabanxx_2
-- ----------------------------
DROP TABLE IF EXISTS `wp_jawabanxx_2`;
CREATE TABLE `wp_jawabanxx_2`  (
  `Urutan` int(11) NOT NULL AUTO_INCREMENT,
  `Urut` int(11) NOT NULL,
  `XNomerSoal` int(11) NOT NULL,
  `XKodeUjian` varchar(25) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL,
  `XTokenUjian` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XA` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XB` int(1) NOT NULL,
  `XC` int(1) NOT NULL,
  `XD` int(1) NOT NULL,
  `XE` int(1) NOT NULL,
  `XJawaban` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTemp` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawabanEsai` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJawab` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilaiJawab` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilaiEsai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XRagu` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XMulai` float NOT NULL,
  `XPutar` int(11) NOT NULL,
  `XMulaiV` float NOT NULL,
  `XPutarV` int(11) NOT NULL,
  `XTglJawab` date NOT NULL,
  `XJamJawab` time(0) NOT NULL,
  `XKunciJawaban` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XUserJawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Campur` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSemester` int(1) NOT NULL,
  `XUserPeriksa` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTglPeriksa` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJamPeriksa` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKD` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bobot` varchar(6) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `idserver` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urutan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
