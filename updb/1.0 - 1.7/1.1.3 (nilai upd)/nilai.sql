ALTER TABLE `dbpusat`.`nilai` 
ADD COLUMN `mapel` varchar(50) NULL AFTER `nik_siswa`,
ADD COLUMN `kelas` varchar(50) NULL AFTER `mapel`,
ADD COLUMN `guru` varchar(50) NULL AFTER `kelas`;

ALTER TABLE `dbpusat`.`nilai` 
MODIFY COLUMN `id_kd` varchar(50) NULL DEFAULT NULL AFTER `guru`;

ALTER TABLE `dbpusat`.`wp_paketsoal` 
ADD COLUMN `id_kd` int(11) NULL AFTER `semester`;


ALTER TABLE `absensi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `absensikelas` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `absensipiket` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `berita_acara` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `dataizin` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `datanilaiwali` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `datapelanggaran` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `datapemilihan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `deskripsi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `deskripsipsiko` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `ekstrakurikuler` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `inf_lokasi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `jenispelanggaran` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kandidat` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kd_guru` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kd_jawaban` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kd_nilai` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kd_paketsoal` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kd_soal` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kognitif` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `komentar` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kompetensi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kompetensi_copy` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `kompetensi_copy1` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `lihat` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `nilai` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `pesan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `phinxlog` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `post` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `prestasi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `psikomotor` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `rpp` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `rpp_master` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `server_sekolah` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `shout_box` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `sikap` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `sikapbk` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `suka_post` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tb_student` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tbl_hubungi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tbl_jadwal_pelajaran` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tbl_materi_ajar` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tbl_pengumuman` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tbl_pengumumanweb` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tbl_user_pesan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `tiketbantuan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_admin` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_agama` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_audio` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_jawaban` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_jawaban_copy1` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_jawabanxx` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_jurusan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_kd` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_kelas` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_khasus` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_level` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_link` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_mapel` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_materi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_nilai` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_paketsoal` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_paketsoalxx` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_pengaturan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_pilihan` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_ruang` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_sesi` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_setid` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_siswa` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_siswa_ujian` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_slide` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_soal` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_soalxx` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_tes` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_ujian` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_upload_file` convert to character set latin1 collate latin1_general_ci;
ALTER TABLE `wp_user` convert to character set latin1 collate latin1_general_ci;