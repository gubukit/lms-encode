INSERT INTO versi (versi) VALUES ("1.2.1");
/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : dbpusat

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 05/12/2019 15:25:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wp_soal_tugas
-- ----------------------------
DROP TABLE IF EXISTS `wp_soal_tugas`;
CREATE TABLE `wp_soal_tugas`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL DEFAULT 1,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XLevel` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNomerSoal` int(50) NOT NULL,
  `XRagu` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTanya` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XAudioTanya` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XVideoTanya` mediumtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab1` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab2` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab3` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab4` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab5` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKunciJawaban` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKategori` int(11) NOT NULL DEFAULT 1,
  `XAcakSoal` enum('A','T') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'A',
  `XAcakOpsi` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XAgama` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XPIL` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bobot` varchar(6) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bahas` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `npsn` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `XKodeMapel`(`XKodeMapel`) USING BTREE,
  INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE,
  INDEX `XKodeKelas`(`XKodeKelas`) USING BTREE,
  INDEX `XNomerSoal`(`XNomerSoal`) USING BTREE,
  INDEX `XKunciJawaban`(`XKunciJawaban`) USING BTREE,
  INDEX `bobot`(`bobot`) USING BTREE,
  FULLTEXT INDEX `XTanya`(`XTanya`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_soal_tugas_pasif
-- ----------------------------
DROP TABLE IF EXISTS `wp_soal_tugas_pasif`;
CREATE TABLE `wp_soal_tugas_pasif`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL DEFAULT 1,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XLevel` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNomerSoal` int(50) NOT NULL,
  `XRagu` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTanya` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XAudioTanya` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XVideoTanya` mediumtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab1` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab2` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab3` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab4` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawab5` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKunciJawaban` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKategori` int(11) NOT NULL DEFAULT 1,
  `XAcakSoal` enum('A','T') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'A',
  `XAcakOpsi` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XAgama` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XPIL` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bobot` varchar(6) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bahas` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `npsn` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kodebersama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `XKodeMapel`(`XKodeMapel`) USING BTREE,
  INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE,
  INDEX `XKodeKelas`(`XKodeKelas`) USING BTREE,
  INDEX `XNomerSoal`(`XNomerSoal`) USING BTREE,
  INDEX `XKunciJawaban`(`XKunciJawaban`) USING BTREE,
  INDEX `bobot`(`bobot`) USING BTREE,
  FULLTEXT INDEX `XTanya`(`XTanya`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
