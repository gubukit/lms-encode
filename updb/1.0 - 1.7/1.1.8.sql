ALTER TABLE `dbpusat`.`wp_jawabanxx_2` 
ADD INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE,
ADD INDEX `XTokenUjian`(`XTokenUjian`) USING BTREE,
ADD INDEX `XUserJawab`(`XUserJawab`) USING BTREE;

ALTER TABLE `dbpusat`.`nilai` COLLATE = latin1_general_ci;

ALTER TABLE `dbpusat`.`nilai` 
MODIFY COLUMN `nik_siswa` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `id`,
MODIFY COLUMN `mapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `nik_siswa`,
MODIFY COLUMN `kelas` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `mapel`,
MODIFY COLUMN `guru` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `kelas`,
MODIFY COLUMN `id_kd` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL AFTER `guru`,
MODIFY COLUMN `catatan` text CHARACTER SET latin1 COLLATE latin1_general_ci NULL AFTER `3.1_final`,
MODIFY COLUMN `status` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL COMMENT 'y= sesuai kkm, n = tidk sesuai kkm' AFTER `2.1`;

ALTER TABLE `dbpusat`.`wp_siswa_ujian` 
ADD COLUMN `XResetUjian` tinyint(2) NULL DEFAULT 0 AFTER `XGetIP`;

ALTER TABLE `dbpusat`.`wp_modul` 
MODIFY COLUMN `Cover` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `XTipe`,
MODIFY COLUMN `File` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `Cover`;

ALTER TABLE `dbpusat`.`kompetensi` 
MODIFY COLUMN `kompetensi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `kodepaket`,
MODIFY COLUMN `ringkas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `kompetensi`;