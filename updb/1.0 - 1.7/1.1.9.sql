ALTER TABLE `dbpusat`.`wp_paketsoal` 
ADD COLUMN `status_guru` int(11) NULL DEFAULT 1 AFTER `main`;

INSERT INTO versi (versi) VALUES ("1.1.9");

/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : dbujian

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 18/11/2019 10:38:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wp_tugas_nilai
-- ----------------------------
DROP TABLE IF EXISTS `wp_tugas_nilai`;
CREATE TABLE `wp_tugas_nilai`  (
  `Urut` int(11) NOT NULL AUTO_INCREMENT,
  `XNomerUjian` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNIK` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeUjian` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTokenUjian` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTgl` date NOT NULL,
  `XJumSoal` int(11) NOT NULL,
  `XBenar` int(11) NOT NULL,
  `XSalah` int(11) NOT NULL,
  `XNilai` varchar(11) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XPersenPil` float NOT NULL,
  `XPersenEsai` float NOT NULL,
  `XEsai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTotalNilai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSemester` int(11) NOT NULL,
  `XKodeSekolah` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XStatus` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKD` varchar(11) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XPIL` int(5) NOT NULL,
  `totsatu` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totdua` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tottiga` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totempat` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totlima` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totenam` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tottujuh` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totdela` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totsembi` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totsepul` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totsatux` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totduax` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tottigax` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totempatx` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totlimax` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totenamx` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tottujuhx` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totdelax` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totsembix` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `totsepulx` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `idserver` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urut`) USING BTREE,
  INDEX `XNomerUjian`(`XNomerUjian`) USING BTREE,
  INDEX `XTokenUjian`(`XTokenUjian`) USING BTREE,
  INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE,
  INDEX `XKodeKelas`(`XKodeKelas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wp_tugas_nisn
-- ----------------------------
DROP TABLE IF EXISTS `wp_tugas_nisn`;
CREATE TABLE `wp_tugas_nisn`  (
  `Urutan` int(11) NOT NULL AUTO_INCREMENT,
  `Urut` int(11) NOT NULL,
  `XNomerSoal` int(11) NOT NULL,
  `XKodeUjian` varchar(25) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeKelas` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJurusan` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeMapel` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSoal` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJenisSoal` int(11) NOT NULL,
  `XTokenUjian` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XA` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XB` int(1) NOT NULL,
  `XC` int(1) NOT NULL,
  `XD` int(1) NOT NULL,
  `XE` int(1) NOT NULL,
  `XJawaban` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTemp` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJawabanEsai` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeJawab` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilaiJawab` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XNilaiEsai` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XRagu` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XMulai` float NOT NULL,
  `XPutar` int(11) NOT NULL,
  `XMulaiV` float NOT NULL,
  `XPutarV` int(11) NOT NULL,
  `XTglJawab` date NOT NULL,
  `XJamJawab` time(0) NOT NULL,
  `XKunciJawaban` longtext CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XUserJawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Campur` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSetId` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XSemester` int(1) NOT NULL,
  `XUserPeriksa` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XTglPeriksa` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XJamPeriksa` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKodeSekolah` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `XKD` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `bobot` varchar(6) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `idserver` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Urutan`) USING BTREE,
  INDEX `XKodeSoal`(`XKodeSoal`) USING BTREE,
  INDEX `XTokenUjian`(`XTokenUjian`) USING BTREE,
  INDEX `XUserJawab`(`XUserJawab`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
