ALTER TABLE `wp_soal_pasif`
ENGINE=InnoDB;

ALTER TABLE `wp_soal`
MODIFY COLUMN `XTanya`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `XRagu`;

ALTER TABLE `wp_soal_pasif`
MODIFY COLUMN `XTanya`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `XRagu`;

ALTER TABLE `wp_soal_tugas`
MODIFY COLUMN `XTanya`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `XRagu`;

ALTER TABLE `wp_soal_tugas_pasif`
MODIFY COLUMN `XTanya`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `XRagu`;

ALTER TABLE `wp_paketsoal`
ENGINE=InnoDB;

INSERT INTO versi (versi) VALUES ("1.7");