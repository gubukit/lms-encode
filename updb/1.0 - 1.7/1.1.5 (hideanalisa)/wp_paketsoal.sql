ALTER TABLE `dbpusat`.`wp_paketsoal` 
ADD COLUMN `hideanalisa` tinyint(2) NULL DEFAULT 0 AFTER `id_kd`;

ALTER TABLE `dbpusat`.`wp_paketsoal` 
MODIFY COLUMN `tingkat` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL AFTER `XTglBuat`;

INSERT INTO versi (versi) VALUES ("1.1.5");
