ALTER TABLE `dbpusat`.`x_modul_file` 
MODIFY COLUMN `type` enum('file','youtube','script') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `id_modul`,
MODIFY COLUMN `file` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `type`;

INSERT INTO versi (versi) VALUES ("2.4");